package com.quoc.coroutine.ui.detail

import com.quoc.coroutine.shared.data.repository.ImageRepository
import com.quoc.coroutine.shared.domain.model.ImageModel
import com.quoc.coroutine.shared.domain.usecase.GetImageDetailUseCase
import com.quoc.coroutine.test.BaseTest
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest : BaseTest() {

    @Mock
    lateinit var repository: ImageRepository
    private lateinit var getImageDetailUseCase: GetImageDetailUseCase
    private lateinit var viewModel: DetailViewModel

    @Before
    fun init() {
        getImageDetailUseCase = GetImageDetailUseCase(repository, dispatcher)
        viewModel = DetailViewModel(getImageDetailUseCase)
    }

    @Test
    fun getImageDetail_returnValidValue() = runTest {
        val model = ImageModel(
            "1", "John", 1, 1, "url", "download_url", 2
        )
        whenever(repository.getLocalImage(model.id)).thenReturn(model)
        viewModel.getDetail(model.id)
        delay(100) // Wait a bit to to load data
        val data = viewModel.imageDetail.value
        assertEquals(model, data)
    }

}