package com.quoc.coroutine.di

import com.quoc.coroutine.shared.data.paging.ImagePagingDataSource
import com.quoc.coroutine.shared.data.paging.ImagePagingDataSourceImpl
import com.quoc.coroutine.shared.data.repository.ImageRepository
import com.quoc.coroutine.shared.data.repository.ImageRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindImageRepository(
        imageRepositoryImpl: ImageRepositoryImpl
    ): ImageRepository

    @Binds
    abstract fun bindImagePagingDataSource(
        imagePagingDataSourceImpl: ImagePagingDataSourceImpl
    ): ImagePagingDataSource

}