package com.quoc.coroutine.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.LoadStates
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentHomeBinding
import com.quoc.coroutine.paging.asMergedLoadStates
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.util.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override val viewModel: HomeViewModel by viewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = { inflater, parent, attachToParent ->
            FragmentHomeBinding.inflate(inflater, parent, attachToParent)
        }

    private var imageAdapter by autoCleared<ImagePagingAdapter>()

    override fun setupView() {
        imageAdapter = ImagePagingAdapter {
            findNavController().navigate(HomeFragmentDirections.detail(it.id))
        }

        val linearLayoutManager = LinearLayoutManager(requireContext())
        with(binding.recyclerView) {
            layoutManager = linearLayoutManager
            isNestedScrollingEnabled = false
            adapter = imageAdapter.withLoadStateFooter(
                footer = ImageLoadStateAdapter()
            )
        }
    }

    override fun bindViewEvents() {
        binding.swipeLayout.setOnRefreshListener {
            imageAdapter.refresh()
        }

        imageAdapter.loadStateFlow bindTo ::displayLoadState

        imageAdapter.loadStateFlow
            // Use a state-machine to track LoadStates such that we only transition to
            // NotLoading from a RemoteMediator load if it was also presented to UI.
            .asMergedLoadStates()
            .distinctUntilChangedBy { it.refresh }
            .filter { it.refresh is LoadState.NotLoading } bindTo ::scrollToTop
    }

    override fun bindViewModel() {
        viewModel.images bindTo ::displayImages
        viewModel.error bindTo ::toast
    }

    private fun displayLoadState(states: CombinedLoadStates) {
        // Hide SwipeRefreshLayout
        if (binding.swipeLayout.isRefreshing) {
            binding.swipeLayout.isRefreshing = states.mediator?.refresh is LoadState.Loading
        }
    }

    private fun scrollToTop(states: LoadStates) {
        // Scroll to top is synchronous with UI updates, even if remote load was triggered.
        binding.recyclerView.scrollToPosition(0)
    }

    private fun displayImages(pagingData: PagingData<ImageEntity>) {
        lifecycleScope.launch {
            imageAdapter.submitData(pagingData)
        }
    }

    override fun initData() {
        viewModel.getImages()
    }

}