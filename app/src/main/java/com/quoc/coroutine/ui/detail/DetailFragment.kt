package com.quoc.coroutine.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentDetailBinding
import com.quoc.coroutine.di.GlideApp
import com.quoc.coroutine.shared.domain.model.ImageModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    @Inject
    lateinit var baseUrl: String
    private val params: DetailFragmentArgs by navArgs()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentDetailBinding
        get() = { layoutInflater, viewGroup, attachToParent ->
            FragmentDetailBinding.inflate(layoutInflater, viewGroup, attachToParent)
        }

    override val viewModel: DetailViewModel by viewModels()

    override fun setupView() {}

    override fun bindViewEvents() {
        binding.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun bindViewModel() {
        viewModel.imageDetail bindTo ::displayImageDetail
        viewModel.error bindTo ::toast
    }

    private fun displayImageDetail(imageModel: ImageModel) {
        val url = imageModel.getThumbnailUrl(baseUrl)
        if (URLUtil.isNetworkUrl(url)) {
            GlideApp.with(requireContext())
                .load(url)
                .into(binding.ivImage)
        }
        binding.tvAuthor.text = imageModel.author
    }

    override fun initData() {
        if (params.imageId.isNotEmpty()) {
            viewModel.getDetail(params.imageId)
        }
    }

}