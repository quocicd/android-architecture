package com.quoc.coroutine.ui.home

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.quoc.coroutine.base.BaseViewModel
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.domain.usecase.GetImagesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getImagesUseCase: GetImagesUseCase
) : BaseViewModel() {

    private val _images = MutableStateFlow<PagingData<ImageEntity>>(PagingData.empty())
    val images: StateFlow<PagingData<ImageEntity>> = _images

    fun getImages() {
        viewModelScope.launch {
            getImagesUseCase.invoke(Any())
                .catch { handleError(it) }
                .cachedIn(viewModelScope)
                .collect {
                    _images.value = it
                }
        }
    }
}