package com.quoc.coroutine.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.quoc.coroutine.BuildConfig
import com.quoc.coroutine.databinding.ItemImageBinding
import com.quoc.coroutine.di.GlideApp
import com.quoc.coroutine.shared.data.db.entity.ImageEntity

class ImagePagingAdapter(
    private val action: (ImageEntity) -> Unit
) : PagingDataAdapter<ImageEntity,
        ImagePagingAdapter.ImageViewHolder>(HISTORY_COMPARATOR) {

    companion object {
        val HISTORY_COMPARATOR = object : DiffUtil.ItemCallback<ImageEntity>() {
            override fun areItemsTheSame(
                oldItem: ImageEntity,
                newItem: ImageEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ImageEntity,
                newItem: ImageEntity
            ): Boolean {
                return oldItem.url == newItem.url
                        && oldItem.author == newItem.author
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ImageViewHolder {
        return ImageViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.bind(item)
        holder.itemView.setOnClickListener {
            action.invoke(item)
        }
    }

    class ImageViewHolder(
        private val binding: ItemImageBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun create(parent: ViewGroup): ImageViewHolder {
                return ImageViewHolder(
                    ItemImageBinding.inflate(
                        LayoutInflater.from(
                            parent.context
                        ),
                        parent, false
                    )
                )
            }
        }

        fun bind(item: ImageEntity) {
            val url = item.getThumbnailUrl(BuildConfig.BASE_URL)
            GlideApp.with(itemView.context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(withCrossFade())
                .downsample(DownsampleStrategy.AT_MOST)
                .into(binding.ivImage)
            binding.tvAuthor.text = item.author
        }
    }
}
