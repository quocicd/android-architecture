package com.quoc.coroutine.ui.splash

import androidx.lifecycle.viewModelScope
import com.quoc.coroutine.base.BaseViewModel
import com.quoc.coroutine.testing.AppIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor() : BaseViewModel() {

    private val _showLogo = MutableSharedFlow<Boolean>()
    val showLogo: SharedFlow<Boolean> = _showLogo

    private val _navigateToHome = MutableSharedFlow<Boolean>()
    val navigateToHome: SharedFlow<Boolean> = _navigateToHome

    init {
        viewModelScope.launch {
            AppIdlingResource.increment()
            delay(500)
            _showLogo.emit(true)
            delay(800)
            _navigateToHome.emit(true)
            AppIdlingResource.decrement()
        }
    }

}
