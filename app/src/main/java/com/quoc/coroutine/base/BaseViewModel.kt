package com.quoc.coroutine.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow

open class BaseViewModel : ViewModel() {

    private val _error = Channel<Throwable>(Channel.BUFFERED)
    val error: Flow<Throwable> = _error.receiveAsFlow()

    protected suspend fun handleError(throwable: Throwable) {
        _error.send(throwable)
    }

}