package com.quoc.coroutine.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewbinding.ViewBinding
import com.quoc.coroutine.R
import com.quoc.coroutine.extension.hideSoftKeyboard
import com.quoc.coroutine.ui.dialog.DialogManager
import com.quoc.coroutine.util.autoCleared
import com.quoc.coroutine.util.parseMessage
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

abstract class BaseFragment<VB : ViewBinding> : Fragment(), IBaseFragment {

    @Inject
    lateinit var dialogManager: DialogManager

    protected open val isFullScreen: Boolean = false

    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    var binding by autoCleared<VB>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return bindingInflater.invoke(inflater, container, false).apply {
            binding = this
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (this as? IBaseFragment)?.let {
            setWindowStyle()
            setupView()
            bindViewEvents()
            bindViewModel()
        }
        bindBaseEvents()
        initData()
    }

    private fun bindBaseEvents(){
        view?.setOnClickListener {
            hideSoftKeyboard()
        }
    }

    private fun displayFailureDialog(){
        dialogManager.showDialogTwoButton(
            getString(R.string.error__title_something_went_wrong),
            getString(R.string.error__message_please_try_again)
        ){_, _ -> /* Do nothing */}
    }

    private fun setWindowStyle() {
        if (isFullScreen) {
            hideSystemUI()
        } else {
            showSystemUI()
        }
    }

    private fun hideSystemUI() {
        activity?.run {
            WindowCompat.setDecorFitsSystemWindows(window, false)
            WindowInsetsControllerCompat(window, requireView()).let { controller ->
                controller.hide(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
                controller.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        }
    }

    private fun showSystemUI() {
        activity?.run {
            WindowCompat.setDecorFitsSystemWindows(window, true)
            WindowInsetsControllerCompat(
                window,
                requireView()
            ).show(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
        }
    }

    // Step 2
    protected open fun initData() = Unit

    protected fun toast(throwable: Throwable){
        handleError(throwable){
            toast(it)
        }
    }

    private fun toast(msg: String?) {
        dialogManager.toast(msg)
    }

    private fun handleError(throwable: Throwable, action: (String) -> Unit){
        val (isUnauthorized, message) = throwable.parseMessage(requireContext())
        if (isUnauthorized) {
            displayFailureDialog()
        } else {
            action(message)
        }
    }

    protected inline infix fun <T> Flow<T>.bindTo(crossinline action: (T) -> Unit){
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED){
                collect {
                    action.invoke(it)
                }
            }
        }
    }

}