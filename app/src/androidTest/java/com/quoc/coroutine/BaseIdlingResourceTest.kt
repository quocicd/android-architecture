package com.quoc.coroutine

import androidx.navigation.NavController
import androidx.test.espresso.IdlingRegistry
import com.quoc.coroutine.testing.AppIdlingResource
import org.junit.After
import org.junit.Before
import org.mockito.Mockito

abstract class BaseIdlingResourceTest {

    val navController: NavController = Mockito.mock(NavController::class.java)!!

    @Before
    open fun setUp() {
        IdlingRegistry.getInstance().register(AppIdlingResource.counting)
    }

    @After
    open fun tearDown() {
        IdlingRegistry.getInstance().unregister(AppIdlingResource.counting)
    }
}