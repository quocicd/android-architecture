package com.quoc.coroutine.ui.detail

import androidx.core.os.bundleOf
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.R
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
@LargeTest
@HiltAndroidTest
class DetailFragmentTest : BaseIdlingResourceTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    override fun setUp() {
        super.setUp()
        hiltRule.inject()
    }

    @Test
    fun testInitialState_success() {
        launchFragmentInHiltContainer<DetailFragment>(
            bundleOf("image_id" to "1")
        ) {}
        Espresso.onView(ViewMatchers.withId(R.id.ivBack))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.ivImage))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tvAuthor))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}