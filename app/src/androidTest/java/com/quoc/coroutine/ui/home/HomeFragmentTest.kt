package com.quoc.coroutine.ui.home

import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.R
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
@LargeTest
@HiltAndroidTest
class HomeFragmentTest : BaseIdlingResourceTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    override fun setUp() {
        super.setUp()
        hiltRule.inject()
    }

    @Test
    fun testInitialState_success() {
        launchFragmentInHiltContainer<HomeFragment> {}
        Espresso.onView(withId(R.id.tvTitle))
            .check(ViewAssertions.matches(withText(R.string.app_name)))
        Espresso.onView(withId(R.id.recyclerView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun loadImages_clickFirstItem() {
        launchFragmentInHiltContainer<HomeFragment> {
            Navigation.setViewNavController(this.requireView(), navController)
        }
        // Wait for load items
        TimeUnit.SECONDS.sleep(1)
        // Click the first item
        Espresso.onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ImagePagingAdapter.ImageViewHolder>(
                0,
                ViewActions.click()
            )
        )
        // There is always a item (id = 0) at the first item
        verify(navController).navigate(HomeFragmentDirections.detail("0"))
    }
}