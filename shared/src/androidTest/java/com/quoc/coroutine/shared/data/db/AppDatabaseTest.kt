package com.quoc.coroutine.shared.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.data.db.entity.ImageKeyEntity
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class AppDatabaseTest{

    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun testReadWriteImageDao_success() = runTest {
        val imageDao = db.imageDao()
        val image1 = ImageEntity(
            "1",
            "John",
            1,
            1,
            "url",
            "download_url",
        1
        )
        val image2 = ImageEntity(
            "2",
            "Peter",
            2,
            2,
            "url2",
            "download_url_2",
            2
        )
        val inputList = listOf(image1, image2)

        imageDao.insertAll(inputList)
        val images = imageDao.getAll()

        // Verify all items
        assertEquals(inputList, images)

        // Delete image1
        imageDao.delete(image1)
        // Remain image2
        val remainList = imageDao.getAll()
        // Verify size of table
        assertEquals(1, remainList.size)
        // Verify remain item
        assertEquals(image2, remainList[0])
    }

    @Test
    fun testReadWriteImageKeyDao_success() = runTest {

        val key1 = ImageKeyEntity(ImageKeyEntity.KEY, 1)
        val key2 = ImageKeyEntity(ImageKeyEntity.KEY, 2)
        val key3 = ImageKeyEntity("Key3", 3)

        val imageKeyDao = db.imageKeyDao()
        imageKeyDao.insert(key1)
        imageKeyDao.insert(key2) // Will replace key1
        imageKeyDao.insert(key3)

        val images = imageKeyDao.getAll()

        // Only key2, key3
        assertEquals(2, images.size)

        // Verify key [ImageKeyEntity.KEY]
        val result2 = imageKeyDao.getItem(ImageKeyEntity.KEY)
        assertEquals(key2, result2)

        // Verify key "Key3"
        val result3 = imageKeyDao.getItem(key3.key)
        assertEquals(key3, result3)
    }
}