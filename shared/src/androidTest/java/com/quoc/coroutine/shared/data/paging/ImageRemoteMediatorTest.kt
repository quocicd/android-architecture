package com.quoc.coroutine.shared.data.paging


import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingConfig
import androidx.paging.PagingState
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.quoc.coroutine.shared.data.api.ApiService
import com.quoc.coroutine.shared.data.api.NetworkConst
import com.quoc.coroutine.shared.data.api.response.ImageResponse
import com.quoc.coroutine.shared.data.db.AppDatabase
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.data.db.entity.ImageKeyEntity
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@OptIn(ExperimentalPagingApi::class)
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ImageRemoteMediatorTest {

    private lateinit var db: AppDatabase

    @Mock
    lateinit var apiService: ApiService

    private lateinit var mediator: ImageRemoteMediator

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()
        mediator = ImageRemoteMediator(db, apiService)
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun loadRefreshType_success() = runTest {
        val state = PagingState<Int, ImageEntity>(
            emptyList(), 1,
            PagingConfig(2, 2, false, 4, 10),
            2
        )

        val pageIndex = NetworkConst.PAGING_STARTING_INDEX

        val images = getImages(pageIndex)
        whenever(
            apiService.getNextImages(
                pageIndex,
                state.config.initialLoadSize
            )
        ).thenReturn(images)

        db.imageDao().deleteAll()
        db.imageKeyDao().deleteAll()
        // Refresh
        mediator.load(LoadType.REFRESH, state)
        // Verify result
        verify(apiService).getNextImages(
            pageIndex, state.config.initialLoadSize
        )
        assertEquals(images.size, db.imageDao().getAll().size)
        assertEquals(pageIndex.inc(), db.imageKeyDao().getItem(ImageKeyEntity.KEY)?.index)
    }

    @Test
    fun loadAppendType_success() = runTest {
        val state = PagingState<Int, ImageEntity>(
            emptyList(), 1,
            PagingConfig(2, 2, false, 4, 10),
            2
        )
        db.imageDao().deleteAll()
        db.imageKeyDao().deleteAll()

        val pageIndex = 8
        db.imageKeyDao().insert(ImageKeyEntity(ImageKeyEntity.KEY, pageIndex))

        // Append
        val images = getImages(pageIndex)
        whenever(
            apiService.getNextImages(
                pageIndex,
                state.config.pageSize
            )
        ).thenReturn(images)
        mediator.load(LoadType.APPEND, state)
        // Verify result
        verify(apiService).getNextImages(
            pageIndex, state.config.pageSize
        )
        assertEquals(images.size, db.imageDao().getAll().size)
        assertEquals(pageIndex.inc(), db.imageKeyDao().getItem(ImageKeyEntity.KEY)?.index)
    }

    private fun getImages(page: Int): List<ImageResponse> {
        val index = page * 2
        val image1 = ImageResponse(
            "$index",
            "John",
            1,
            1,
            "url",
            "download_url",
        )
        val image2 = ImageResponse(
            "${index + 1}",
            "Peter",
            2,
            2,
            "url2",
            "download_url_2",
        )
        val image3 = ImageResponse(
            "${index + 2}",
            "Hana",
            3,
            3,
            "url3",
            "download_url_3",
        )
        return listOf(image1, image2, image3)
    }
}