package com.quoc.coroutine.shared.data.repository


import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.quoc.coroutine.shared.data.api.ApiService
import com.quoc.coroutine.shared.data.api.response.ImageResponse
import com.quoc.coroutine.shared.data.db.AppDatabase
import com.quoc.coroutine.shared.data.db.ImageDao
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ImageRepositoryTest {

    private lateinit var db: AppDatabase
    private lateinit var imageDao: ImageDao
    @Mock lateinit var apiService: ApiService

    private lateinit var repository: ImageRepository

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()
        imageDao = db.imageDao()
        repository = ImageRepositoryImpl(imageDao, apiService)
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun getLocalImage_returnValidValue() = runTest {
        db.clearAllTables()
        val image1 = ImageEntity(
            "1",
            "John",
            1,
            1,
            "url",
            "download_url",
            1
        )
        val image2 = ImageEntity(
            "2",
            "Peter",
            2,
            2,
            "url2",
            "download_url_2",
            2
        )
        imageDao.insert(image1)
        imageDao.insert(image2)

        val image = repository.getLocalImage(image1.id)
        // Verify result
        assertEquals(image1.author, image?.author)

        // Verify a null item
        val noImage = repository.getLocalImage("3")
        assertThat(noImage, CoreMatchers.nullValue())
    }

    @Test
    fun storeImage_success()= runTest {
        val image = ImageEntity(
            "1",
            "John",
            1,
            1,
            "url",
            "download_url",
            1
        )

        // Clear table
        imageDao.deleteAll()
        // Check empty
        assertEquals(0, imageDao.getAll().size)
        // Store item
        repository.storeImage(image)
        // Verify result
        assertEquals(1, imageDao.getAll().size)
        assertEquals(image, imageDao.getImage(image.id))
    }

    @Test
    fun getRemoteImage_returnValidValue() = runTest {
        val image = ImageResponse(
            "1",
            "John",
            1,
            1,
            "url",
            "download_url",
        )
        whenever(apiService.detail(image.id)).thenReturn(image)
        val result = repository.getRemoteImage(image.id)
        // Verify result
        verify(apiService).detail(image.id)
        assertEquals(image, result)
    }


}