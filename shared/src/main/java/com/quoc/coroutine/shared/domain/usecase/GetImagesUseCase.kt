package com.quoc.coroutine.shared.domain.usecase

import androidx.paging.PagingData
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.data.paging.ImagePagingDataSource
import com.quoc.coroutine.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetImagesUseCase @Inject constructor(
    private val dataSource: ImagePagingDataSource,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : FlowPagingUseCase<Any, ImageEntity>(dispatcher) {

    override fun execute(parameters: Any): Flow<PagingData<ImageEntity>> {
        return dataSource.getImages()
    }

}