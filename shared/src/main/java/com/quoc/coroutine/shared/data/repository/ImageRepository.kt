package com.quoc.coroutine.shared.data.repository

import com.quoc.coroutine.shared.data.api.response.ImageResponse
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.domain.model.ImageModel

interface ImageRepository {

    suspend fun getLocalImage(id: String): ImageModel?

    suspend fun getRemoteImage(id: String): ImageResponse

    suspend fun storeImage(imageEntity: ImageEntity)
}