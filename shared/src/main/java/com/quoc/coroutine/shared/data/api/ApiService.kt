package com.quoc.coroutine.shared.data.api

import com.quoc.coroutine.shared.data.api.response.ImageResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("/v2/list")
    suspend fun getNextImages(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): List<ImageResponse>

    @GET("/id/{id}/info")
    suspend fun detail(@Path("id") id: String): ImageResponse
}