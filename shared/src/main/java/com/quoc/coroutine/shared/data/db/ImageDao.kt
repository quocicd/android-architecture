package com.quoc.coroutine.shared.data.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import com.quoc.coroutine.shared.data.db.entity.ImageEntity

@Dao
interface ImageDao : BaseDao<ImageEntity> {

    @Query("SELECT * FROM ImageEntity ORDER BY updated_at ASC")
    fun getAllImages(): PagingSource<Int, ImageEntity>

    @Query("SELECT * FROM ImageEntity ORDER BY updated_at ASC")
    fun getAll(): List<ImageEntity> // For unit test, equivalent to [getAllImages()]

    @Query("SELECT * FROM ImageEntity WHERE id =:id")
    suspend fun getImage(id: String): ImageEntity?

    @Query("DELETE FROM ImageEntity")
    suspend fun deleteAll()
}