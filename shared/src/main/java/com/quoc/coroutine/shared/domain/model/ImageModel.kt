package com.quoc.coroutine.shared.domain.model


data class ImageModel(
    val id: String,
    val author: String,
    val width: Int,
    val height: Int,
    val url: String? = null,
    val downloadUrl: String? = null,
    val updatedAt: Long
) {
    companion object {
        fun createEmpty() = ImageModel("", "", -1, -1, "", "", -1)
    }

    fun getThumbnailUrl(baseUrl: String) = "$baseUrl/id/$id/100/100.jpg"
}
