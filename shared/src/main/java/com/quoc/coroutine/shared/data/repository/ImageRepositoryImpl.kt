package com.quoc.coroutine.shared.data.repository

import com.quoc.coroutine.shared.data.api.ApiService
import com.quoc.coroutine.shared.data.api.response.ImageResponse
import com.quoc.coroutine.shared.data.db.ImageDao
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.domain.model.ImageModel
import javax.inject.Inject

class ImageRepositoryImpl @Inject constructor(
    private val imageDao: ImageDao,
    private val apiService: ApiService
) : ImageRepository {

    override suspend fun getLocalImage(id: String): ImageModel? {
        return imageDao.getImage(id)?.toModel()
    }

    override suspend fun getRemoteImage(id: String): ImageResponse {
        return apiService.detail(id)
    }

    override suspend fun storeImage(imageEntity: ImageEntity) {
        imageDao.insert(imageEntity)
    }

}