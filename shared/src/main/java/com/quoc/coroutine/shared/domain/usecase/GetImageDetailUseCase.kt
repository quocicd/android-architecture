package com.quoc.coroutine.shared.domain.usecase

import com.quoc.coroutine.shared.data.repository.ImageRepository
import com.quoc.coroutine.shared.di.IoDispatcher
import com.quoc.coroutine.shared.domain.model.ImageModel
import com.quoc.coroutine.shared.lib.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetImageDetailUseCase @Inject constructor(
    private val repository: ImageRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : FlowUseCase<String, ImageModel>(dispatcher) {

    override suspend fun execute(parameters: String): Flow<Resource<ImageModel>> {
        return flow {
            // Get local item
            var item = repository.getLocalImage(parameters)
            if (item == null) {
                // Fetch item remotely
                val entity = repository.getRemoteImage(parameters)
                    .toEntity(System.nanoTime()).also {
                        // Store in local database
                        repository.storeImage(it)
                    }
                item = entity.toModel()
            }
            // Return result
            emit(Resource.Success(item))
        }
    }
}