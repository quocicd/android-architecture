package com.quoc.coroutine.shared.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.quoc.coroutine.shared.domain.model.ImageModel

@Entity
data class ImageEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "author")
    val author: String,
    @ColumnInfo(name = "width")
    val width: Int,
    @ColumnInfo(name = "height")
    val height: Int,
    @ColumnInfo(name = "url")
    val url: String? = null,
    @ColumnInfo(name = "download_url")
    val downloadUrl: String? = null,
    @ColumnInfo(name = "updated_at")
    val updatedAt: Long
) {
    fun toModel() = ImageModel(id, author, width, height, url, downloadUrl, updatedAt)

    fun getThumbnailUrl(baseUrl: String) = "$baseUrl/id/$id/100/100.jpg"
}