package com.quoc.coroutine.shared.data.db

import androidx.room.Dao
import androidx.room.Query
import com.quoc.coroutine.shared.data.db.entity.ImageKeyEntity

@Dao
interface ImageKeyDao : BaseDao<ImageKeyEntity> {

    @Query("SELECT * FROM paging_keys")
    fun getAll(): List<ImageKeyEntity>

    @Query("SELECT * FROM paging_keys WHERE key =:key LIMIT 1")
    suspend fun getItem(key: String = ImageKeyEntity.KEY): ImageKeyEntity?

    @Query("DELETE FROM paging_keys")
    suspend fun deleteAll()
}
