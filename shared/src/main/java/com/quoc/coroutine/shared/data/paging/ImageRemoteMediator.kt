package com.quoc.coroutine.shared.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.quoc.coroutine.shared.data.api.ApiService
import com.quoc.coroutine.shared.data.api.NetworkConst
import com.quoc.coroutine.shared.data.db.AppDatabase
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.data.db.entity.ImageKeyEntity

import okio.IOException
import retrofit2.HttpException
import timber.log.Timber

@ExperimentalPagingApi
class ImageRemoteMediator(
    private val db: AppDatabase,
    private val apiService: ApiService
) : RemoteMediator<Int, ImageEntity>() {

    private val imageDao = db.imageDao()
    private val keyDao = db.imageKeyDao()

    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, ImageEntity>
    ): MediatorResult {

        try {
            val page = when (loadType) {
                LoadType.REFRESH -> NetworkConst.PAGING_STARTING_INDEX
                LoadType.PREPEND -> return MediatorResult.Success(true)
                LoadType.APPEND -> {
                    db.withTransaction {
                        keyDao.getItem()?.index ?: NetworkConst.PAGING_STARTING_INDEX
                    }
                }
            }

            Timber.d("Loading Page: $page, type: $loadType")
            val limit = when (loadType) {
                LoadType.REFRESH -> state.config.initialLoadSize
                else -> state.config.pageSize
            }

            val items = apiService.getNextImages(page, limit)

            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    imageDao.deleteAll()
                    keyDao.deleteAll()
                }
                imageDao.insertAll(
                    items.map { it.toEntity(System.nanoTime()) }
                )

                keyDao.insert(ImageKeyEntity(ImageKeyEntity.KEY, page.inc()))
            }

            return MediatorResult.Success(endOfPaginationReached = items.isEmpty())
        } catch (exception: IOException) {
            exception.printStackTrace()
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            exception.printStackTrace()
            return MediatorResult.Error(exception)
        }
    }
}
