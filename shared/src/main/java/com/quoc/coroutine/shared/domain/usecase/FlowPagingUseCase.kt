package com.quoc.coroutine.shared.domain.usecase

import androidx.paging.PagingData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

/**
 * Executes business logic in its execute method
 */
abstract class FlowPagingUseCase<in P, R : Any>(private val dispatcher: CoroutineDispatcher) {
    operator fun invoke(parameters: P): Flow<PagingData<R>> = execute(parameters)
        .flowOn(dispatcher)

    protected abstract fun execute(parameters: P): Flow<PagingData<R>>
}
