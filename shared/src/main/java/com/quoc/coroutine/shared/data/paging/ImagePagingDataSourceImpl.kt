package com.quoc.coroutine.shared.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.quoc.coroutine.shared.data.api.ApiService
import com.quoc.coroutine.shared.data.api.NetworkConst
import com.quoc.coroutine.shared.data.db.AppDatabase
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


interface ImagePagingDataSource {
    fun getImages(): Flow<PagingData<ImageEntity>>
}

class ImagePagingDataSourceImpl @Inject constructor(
    private val db: AppDatabase,
    private val apiService: ApiService
) : ImagePagingDataSource {

    @OptIn(ExperimentalPagingApi::class)
    override fun getImages(): Flow<PagingData<ImageEntity>> {
        return Pager(
            config = PagingConfig(
                NetworkConst.PAGING_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = ImageRemoteMediator(db, apiService)
        ) {
            db.imageDao().getAllImages()
        }.flow
    }
}