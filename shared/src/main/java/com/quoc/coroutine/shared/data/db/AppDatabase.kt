package com.quoc.coroutine.shared.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.quoc.coroutine.shared.data.db.entity.ImageEntity
import com.quoc.coroutine.shared.data.db.entity.ImageKeyEntity

@Database(
    entities = [
        ImageEntity::class,
        ImageKeyEntity::class
    ], version = 1, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun imageDao(): ImageDao
    abstract fun imageKeyDao(): ImageKeyDao
}