package com.quoc.coroutine.shared.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "paging_keys")
data class ImageKeyEntity(
    @PrimaryKey
    @ColumnInfo(name = "key")
    val key: String,

    @ColumnInfo(name = "index")
    val index: Int,
){
    companion object{
        const val KEY = "IMAGE_KEY"
    }
}
