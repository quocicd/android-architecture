package com.quoc.coroutine.shared.domain.usecase

import app.cash.turbine.test
import com.quoc.coroutine.shared.data.repository.ImageRepository
import com.quoc.coroutine.shared.lib.Resource
import com.quoc.coroutine.shared.util.TestUtils
import com.quoc.coroutine.test.BaseTest
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.exceptions.base.MockitoException
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetImageDetailUseCaseTest : BaseTest() {

    @Test
    fun getImageDetail_returnLocalData() = runTest {
        val id = "1"
        val imageModel = TestUtils.getImageModel(id)
        // Return local image
        val repository = mock<ImageRepository>{
            on{ runBlocking { getLocalImage(id) }}.thenReturn(imageModel)
        }
        val useCase = GetImageDetailUseCase(repository, dispatcher)
        // Trigger and verify result
        useCase.invoke(id).test {
            val result = awaitItem() as Resource.Success
            assertEquals(imageModel, result.data)
            awaitComplete()
        }
    }

    @Test
    fun getImageDetail_returnRemoteData() = runTest {
        val id = "10"
        val response = TestUtils.getImageResponse(id)

        val repository = mock<ImageRepository>{
            // Local image is not available
            on{ runBlocking { getLocalImage(id) }}.thenReturn(null)
            // Return remote image
            on{ runBlocking { getRemoteImage(id) }}.thenReturn(response)
        }
        val useCase = GetImageDetailUseCase(repository, dispatcher)
        // Trigger and verify result
        useCase.invoke(id).test {
            val result = awaitItem() as Resource.Success
            assertEquals(id, result.data.id)
            awaitComplete()
        }
    }

    @Test
    fun getImageDetail_returnError() = runTest {
        val id = "2"
        val error = MockitoException("aaa!")
        val repository = mock<ImageRepository>{
            // Local image is not available
            on{ runBlocking { getLocalImage(id) }}.thenReturn(null)
            // Remote image not available
            on{ runBlocking { getRemoteImage(id) }}.thenThrow(error)
        }

        val useCase = GetImageDetailUseCase(repository, dispatcher)
        // Trigger and observe error
        useCase.invoke(id).test {
            val result = awaitItem() as Resource.Error
            assertThat(result.exception.message, CoreMatchers.equalTo(error.message))
            awaitComplete()
        }
    }
}