package com.quoc.coroutine.shared.util

import com.quoc.coroutine.shared.data.api.response.ImageResponse
import com.quoc.coroutine.shared.domain.model.ImageModel

object TestUtils {

    fun getImageResponse(id: String) = ImageResponse(
        id, "John", 1, 1, "url", "download_url"
    )

    fun getImageModel(id: String) = ImageModel(
        id, "John", 1, 1, "url", "download_url", 2
    )
}