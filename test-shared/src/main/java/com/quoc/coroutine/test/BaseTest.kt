package com.quoc.coroutine.test

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestDispatcher
import org.junit.Rule

@OptIn(ExperimentalCoroutinesApi::class)
open class BaseTest {

    @get:Rule
    val coroutineRule = MainTestRule()

    val dispatcher: TestDispatcher
        get() = coroutineRule.testDispatcher
}
