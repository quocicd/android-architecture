## Android Architecture

### Introduction
Android Architecture is an Android application template that is built with basic functionalities and focused on architecture design to apply for all applications.
### Tech Stack
1. [MVVM](https://developer.android.com/topic/libraries/architecture/viewmodel) and [Clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) for app architecture
2. [Kotlin Flow](https://developer.android.com/kotlin/flow) can helps a stream of data that can be computed asynchronously
3. [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) for dependencies injection
4. [Room](https://developer.android.com/jetpack/androidx/releases/room) for local database
5. [DataStore](https://developer.android.com/topic/libraries/architecture/datastore) for local storage with simple data
6. [Encrypted Shared Preferences](https://developer.android.com/reference/androidx/security/crypto/EncryptedSharedPreferences) for secured local storage
7. [Paging 3](https://developer.android.com/jetpack/androidx/releases/paging) helps loading and displaying pages of data from a larger dataset
7. [Retrofit](https://square.github.io/retrofit/) for networking
8. [Glide](https://github.com/bumptech/glide) for loading and caching images
9. [Mockito](https://github.com/mockito/mockito) for mocking in unit test and instrumentation tests
10. [Espresso](https://developer.android.com/training/testing/espresso) for UI tests

### Install

1. Pull source code [Android Architecture](https://gitlab.com/quocicd/android-architecture).

```bash
git clone git@gitlab.com:quocicd/android-architecture.git
```
2. Download and open [Android studio](https://developer.android.com/studio)
3. [Build and run](https://developer.android.com/studio/run) the application

### Testing
1. Run all tests
```bash
./gradlew test
```
2. Run all Android tests on a connected device
```bash
./gradlew connectedAndroidTest
```
### Screens

#### Home Screen
- Loading images from [Lorem Picsum](https://picsum.photos/)
- Paging
- Caching with Room database

#### Detail screen
- Display image details

### Other features
- Write unit test, integration test, UI test
- Config Gitlab-ci